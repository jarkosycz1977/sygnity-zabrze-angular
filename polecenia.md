ng g m playlists --routing true -m app

ng g c playlists/views/playlists-view --export

ng g c playlists/components/items-list
ng g c playlists/components/list-item

ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

npm install bootstrap --save

<!-- https://www.npmjs.com/package/ng-swagger-gen -->

ng g m music-search --routing true -m app

ng g c music-search/views/music-search --export 

ng g c music-search/components/search-form
ng g c music-search/components/search-results

ng g c music-search/components/album-card

ng g i models/Album 


ng g s music-search/services/music-search 

ng g m security -m app

ng g s security/auth

ng g s security/auth-interceptor


ng g m shared -m playlists 

ng g c shared/card --export 


ng g d shared/highlight --export 

 ng g d shared/img-load --export


ng g s playlists/playlists 

http://vavatech.pl/ankieta 