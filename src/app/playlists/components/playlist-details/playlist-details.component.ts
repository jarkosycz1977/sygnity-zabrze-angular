import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class PlaylistDetailsComponent {

  @Input()
  playlist: Playlist 

  @Output()
  edit = new EventEmitter()
  
  onEdit(){
    this.edit.emit()
  }


  constructor() {}

  ngOnInit() {}
}
