import { Component, OnInit } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { PlaylistsService } from "../../playlists.service";
import { Router, ActivatedRoute } from "@angular/router";
import { map, switchMap, tap, withLatestFrom } from "rxjs/operators";
import { combineLatest } from "rxjs";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent implements OnInit {
  mode = "show";
  playlists$ = this.service.playlistsChange;

  selected: Playlist;
  selected$;

  updateSelected() {
    this.selected$ = combineLatest(this.playlists$, this.route.paramMap).pipe(
      map(([p, paramMap]) => paramMap.get("playlist_id")),
      switchMap(id => this.service.getPlaylist(id)),
      tap(playlist => (this.selected = playlist))
    );
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {
    this.updateSelected();
  }

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id]);
  }

  save(draft: Playlist) {
    this.service.save(draft);
    this.mode = 'show'
  }

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  ngOnInit() {}
}
